import java.util.Scanner;
public class Shop
{
	public static void main (String[] args)
	{
		Scanner reader = new Scanner(System.in);	
		
		Instruments[] products = new Instruments[4];
		for (int i=0; 4 > i; i++)
		{
			products[i] = new Instruments();
			System.out.println("Please input the name of the musical instrument:");
			products[i].name = reader.next();
			System.out.println("Now the type of this musical instrument:");
			products[i].type = reader.next();
			System.out.println("Finally input its difficulty (on a scale of 1 to 5)");
		    products[i].difficulty = reader.nextInt();
		}
		System.out.println("Name:" + products[3].name);
		System.out.println("Type:" + products[3].type);
		System.out.println("Difficulty:" + products[3].difficulty);
		
		Instruments instrument = new Instruments();
		instrument.difficultyExplanation(products[3].difficulty);
	}
}